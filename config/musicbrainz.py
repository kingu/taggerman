import musicbrainzngs as mb

#
# This is *experimental* code for musicbrainz in Taggerman
#

def init(args):
    print("ASDF")

def musicbrainz(f, tgm, args):
    ua = "Taggerman ({})".format(tgm.os)
    mb.set_useragent(ua, tgm.version, "https://framagit.org/kingu/taggerman")
    
    result = mb.search_artists(artist=tgm.getTagAsString("artist"))
    for a in result['artist-list']:
        print(u"{id}: {name}".format(id=a['id'], name=a["name"]))
        
