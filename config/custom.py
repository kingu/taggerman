# This file is a sample custom file!
# Place your own custom file in ~/.config/taggerman/custom.py
# You can then copy and modify functions from here or create your own.

import os, re, subprocess, datetime, taglib
from click import secho
from pathlib import Path

# When defined, the custom.init function will be called before the main program is executed but after
# the arguments are parsed.  Arguments (re)defined here have priority over everywhere else.
# The actual order of priority for arguments is as follow:
# 1 - This init function
# 2 - The argument passed on the command line
# 3 - The environment variable
# 4 - The default value (if any)

def init(args):
    if not args.custom1 and (not args.actions or args.actions=="echo"):
        args.actions='ls'

# def custom1(f, fr, args=False):
#     tagref=fr.tag()
#     pf = os.getenv("TAGGERMAN_PRINT_FORMAT","[{year:^4}] {artist:.<25}| {title:<40}| {album:<25}| {genre:<20} {comment}")
#     v = lambda t: getattr(tagref, t, "--empty--")
#     secho (pf.format(artist = v("artist"), title=v("title"), genre = v("genre"), album  = v("album"), year=v("year"), comment=v("comment") ))
#     return True

def custom1(f, tgm, args=False):
    print(tgm.tags)

def custom2(f, fr, args=False):
    tf = taglib.File(f)
    print (tf.tags)
    return True

        
# To call a function from taggerman just use it's name
# i.e. taggerman ~/music/dir -a "play"

def play(f, tgm, args=False):
    # Play the file with the default media player
    os.system('xdg-open "{}"'.format(f))
    return True


def ls(f, tgm, args=False):
    ls = subprocess.run(['ls', '-lh', f], stdout=subprocess.PIPE)
    sls = ls.stdout.decode().split()
    fr = tgm.fref
    #aup = fr.audioProperties()
    print( "{perm} {owner:<6} {size:>6} {type:>4} {bitrate:>5}kb/s {dur} {chan} {srate}Hz {filename}".format(
        perm=sls[0], owner=sls[2], size=sls[4], bitrate=fr.bitrate, type=f.split('.')[-1], dur=str(datetime.timedelta(seconds=fr.length)),
        chan=fr.channels, srate=fr.sampleRate, filename=f
    ) )

def err0r(f, fr, args=False):
    # dummy function that generate an error.
    return { "error": { "label":"This is an error",
                        "message":"It makes me sad.  REDO REDO",
                        "exit":True }}
    
# def setTagsFromFilename(f, fr, args=False):
#     tr=fr.tag()
#     m = re.search("/home/jphil/Music/([^/]+)/\[([0-9][0-9][0-9][0-9])\] ([^/]+).*/(.+)?\.(mp3|mp4|flac|ogg)$",
#                   os.path.abspath(f))
#     if m:
#         setattr(tr, "artist", m.group(1))
#         setattr(tr, "year", int(m.group(2)))
#         setattr(tr, "album", m.group(3))
#         setattr(tr, "title", m.group(4))
#         return { "save":True }
#     else:
#         secho(f)
#         secho("Bad filename format")
#         return False

# def setFilenameFromTitle(f, fr, args=False):
#     p = Path(f)    
#     return { "rename": "{parent}/{num}-{title}{ext}".format(parent=p.parent,
#                                                             num=str(fr.tag().track).zfill(2),
#                                                             title=fr.tag().title,
#                                                             ext=p.suffix) }
             
# def setTitleFromFilename(f, fr, args=False):
#     t=f.split("/")[-1].split(".")[0].strip()
#     del fr.tags["TITLE"]
#     fr.tags["TITLE"] = [ t ]
#     return { "save":True }
    
def stripStringFromTag(f, tgm, args=[]):

    if len(args) == 3:
        old = tgm.getTagAsString(args[1])
        new = old.replace(args[2], '')
        if not old == new:
            tgm.setTag(args[1],new)
        return { "save":True }
    else:
        print("Wrong number of arguments")
        return False
    
def stripStringFromFilename(f, tgm, args=[]):
    if len(args) == 2:
        return { "rename": f.replace(args[1], '') }
    else:
        print("Wrong number of arguments")
        return False
    
# JpopSingles strippers

def stripJpopsinglesFromTitle(f, tgm, args=False):
    return stripStringFromTag(f, tgm, [True, "title", " 「www.jpopsingles.eu」"])
                              
def stripJpopsinglesFromFilename(f, tgm, args=False):
#    nfn = f.replace("「www.jpopsingles.eu」",'')
    return stripStringFromFilename(f, tgm, [True, " 「www.jpopsingles.eu」"])

def stripJpopsingles(f, tgm, args=False):
    a = stripJpopsinglesFromTitle(f, tgm, args)
    b = stripJpopsinglesFromFilename(f, tgm, args)
    a.update(b)
    return a


