# Taggerman

Taglib based Command-line audio file tag editor in Python

## Description

Taggerman is a command line audio file tag editor.  It supports all formats that Taglib support.
It's aim is to be an audio tags power-tool!

# ATTENTION * IMPORTANT

We changed the underlying library. It's still Taglib, but we now connect to it using pytaglib instead if tagpy.

All commits during the transition were done as version 0.0.2. **Don't use anything labelled version 0.0.2!**  Use 0.0.3 or higher.  

## Installation

### Linux

#### 1) Install the requirements

    sudo apt-get install taglib python3-taglib

#### 2) Install taggerman.

- Download Taggerman via the Framagit page and unzip it.
- or, if git is installed, `git clone git@framagit.org:kingu/taggerman.git`
- Proceed to the taggerman directory.

Do the following:

     chmod 755 taggerman
     sudo cp taggerman /usr/local/bin/

#### 3) Customize (optional)

If programming in Python is okay with you.

     mkdir ~/.config/taggerman
     cp custom.py ~/.config/taggerman

Customize this file as you see fit.  Please share your creations.

### Other OSes

At the present, Taggerman on other OSes is to be considered "Not supported".
However, I think it should work just fine on both MacOS or Windows with the WSL or Cygwin installed.

## Usage
taggerman [options]

### Options

#### *[directory]*  *(optional, default=.)*

Directory to scan for audio files.<br>
If empty, the `TAGGERMAN_DIRECTORY` environment variable will be used.<br>
If also empty, the current directory will be used.

*directory* can also be a single file or a playlist (m3u, pls or xspf)

#### -c / --conditions  *(optional, default=Match All)*

Conditions to match files.  Multiple conditions are separated and 'and' or 'or' and parenthesis can also be used.

Each condition is a tagname, an comparator and a value:   i.e. genre=J-Pop

Comparator can be :

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**=**&nbsp;&nbsp;equality<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**~**&nbsp;&nbsp;regexp<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**>**&nbsp;&nbsp;after (*for year and date tags*)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**<**&nbsp;&nbsp;before  (*for year and date tags*)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**!**&nbsp;&nbsp;part of (multi-tag) 

##### Special values:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-empty-&nbsp;&nbsp;will match an empty tag


#### -a / --actions  *(optional, default=echo)*

Actions to perform on the matched files

Multiple actions are separated by ";"   i.e.  "artist=Perfume;genre=J-Pop"

Actions can be:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**echo**&nbsp;&nbsp;Print out the filename<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**print**&nbsp;&nbsp;Print out the a formated view of the matched files><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**show *x***&nbsp;&nbsp;Print out the content of tag **x**  *i.e. "show title"*<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**format *x***&nbsp;&nbsp;Print out the formated string x  i.e, 'format "[{year}] {title:<25} ({artist})"'<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**tagname=value**&nbsp;&nbsp;Set tagname to value  i.e. "artist=BiSH"<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**tagname>value**&nbsp;&nbsp;Add value to tagname (multi-tag)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**tagname<value**&nbsp;&nbsp;Remove value to tagname (multi-tag)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**reset *x***&nbsp;&nbsp;Reset of tag **x**  (i.e. `reset genre` will empty the genre tag)<br>

##### Special values:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-empty-&nbsp;&nbsp;will match an empty tag
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-ask-&nbsp;&nbsp;will prompt you for the value. 

##### Multi-Tag

The multi-tag separator can be customized with the `TAGGERMAN_MULTITAG_SEPARATOR` environment variable.
This is used to separate multiple values for a tag when the container only acept one.  You probably want to let the default, ";".

#### Custom action

In addition to the build-in, custom actions can be created and saved in ~/.config/taggerman/custom.py
(on Windows it is %APPDATA$\taggerman\custom.py)
  
Any function defined in this file will be called with 3 args: filename, a Taggerman object and the args passed to the function.
  
If the function has to perform something that is beyond it's scope, you can specify further actions in the return value.
  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**save**;&nbsp;&nbsp;{"save":True} will save the tags to the file.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**rename**&nbsp;&nbsp;{"rename":"The/file/new/name") will rename the file to the newname.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**error**&nbsp;&nbsp;{"error":{"label":"There is a problem", "message":"Division by zero" ).   The boolean arg "exit" can also be used for fatal errors. (default is no exit)
   
Never save or rename a file yourself in the custom file. Always use 'save' or 'rename' so the --test and --interactive arguments works as expected.

#### -n / --nocustom

Do not load any custom file.

#### -C / --customfile NAME

Load the custom file named NAME instead of the default "custom".  The argument is just the name of the file. So for "~/.config/taggerman/mycustomfile.py" just write "mycustomfile"

#### -e / --elseAction

Same as --actions but is triggered on file NOT matching the conditions.

#### -s / --silent

Do not display the banner and the working indicator. (do this if you plan to pipe the output)

#### -t / --test 

Destructive action (thoses that write on file) will not be performed but will just print out what it would have been done.

#### -i / --interactive 

User is prompted with a confirm dialog when a file a about to be modified.

#### -v / --verbose  *(optional)*

Verbose will make the program print out more stuff as it work.   Probably useless unless you want to debug something.

#### -[123]  / custom[123]

If a function named "custom1" (or 2 or 3) is present in the currently loaded custom file, it is executed.

## Environment Variables

### TAGGERMAN_DIRECTORY

Default directory to use.  Default to the current directory.   Yes, the default has a default.

### TAGGERMAN_PRINT_FORMAT

The format to use for printing.  Default is `[{year:>4}] {artist:25}| {title:20}| {album:25}| {genre:20}`

### TAGGERMAN_FILE_FORMAT_REGEXP

Regexp to match audio files.
Default to `'\.(mp3|mpc|mpp|mp4|ogg|flac|asf|aiff)$'` 

### TAGGERMAN_MULTITAG_SEPARATOR

Separator for multi-tagging. Default to ';'.

## Usage Examples

List all audio files in current directory and below:

    %> taggerman

Print all files in and below the ~/Music/Perfume directory and set their tags genre to "J-Pop" and artist to "Perfume"

    %> taggerman ~/Music/Perfume -a "print; genre=J-Pop; artist=Perfume"


Set the genre of all file om ~/Music to "J-Pop" if their genre is either "Jpop", "Japanese Pop" or match the regexp "^Domestic".

    %> taggerman ~/Music -c 'genre=Jpop or genre="Japanese Pop" or genre~"^Domestic"' -a "genre=J-Pop"

Fix the genres tag on your Genesis collection

    %> taggerman ~/Music -c 'artist="Genesis" and year<1980' -a 'genre="progressive Rock"' -e 'genre="Pop Rock"'

Please see the wiki for more example: https://framagit.org/kingu/taggerman/wikis/Recipe

## Support/Contact

Mastodon : https://octodon.social/@kingu_platypus_gidora<br>
Twitter  : https://twitter.com/Ghidorah<br>
Facebook : https://www.facebook.com/Taggerman-108364908521084<br>
Email    : tronconneuse@gmail.com

## License

License :: OSI Approved :: GNU General Public License v3 (GPLv3)<br>
This means this program is free to use and share.  Enjoy.

## Project status

Development Status :: 3 - Alpha<br>
This means the program have all of it's functionality present but has not been
throughouly tested yet and erratic behaviour are still possible.
